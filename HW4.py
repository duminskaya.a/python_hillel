'''
Дана довільна строка. Напишіть код, який знайде в ній і виведе на екран кількість слів, які містять дві голосні літери підряд.
'''

vowels = ['a', 'e', 'y', 'u', 'i', 'o',
        'а', 'и', 'е', 'ё', 'о', 'у', 'ы', 'э', 'ю', 'я',
        'а', 'е', 'и', 'і', 'о', 'у', 'є', 'ю', 'я', 'ї']

s = input('Ведіть речення: ')
words = s.split(' ')
print(words)
vowel_count = 0
following_vowels = 0
vowel_words_count = 0

for word in words:
    for letter in word:
        if letter in vowels:
            vowel_count += 1
            following_vowels += 1
        else:
            following_vowels = 0
        if following_vowels == 2:
            vowel_words_count += 1

print(f"Кількість слів з двома гласними підряд: {vowel_words_count}")




'''
Є два довільних числа які відповідають за мінімальну і максимальну ціну. Є Dict з назвами магазинів і цінами: 
{ "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166, 
"the_partner": 38.988, "store": 37.720, "rozetka": 38.003}. Напишіть код, який знайде і виведе на екран назви магазинів, 
ціни яких попадають в діапазон між мінімальною і максимальною ціною. 
'''


prices = {"cito": 47.999, "BB_studio": 42.999, "momo": 49.999,
          "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166,
          "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}

min_price = int(input('Введіть мінімальну ціну: '))
max_price = int(input('Введіть максимальну ціну: '))
for k, v in prices.items():
    if (v > min_price) & (v < max_price):
        print(k)
