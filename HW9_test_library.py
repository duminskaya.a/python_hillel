'''
1
'''

import library

assert library.is_string_capitalized('My name is David') is False
assert library.is_string_capitalized('I love playing') is True
assert library.is_string_capitalized('') is True
assert library.is_string_capitalized('565656') is True

'''
2
'''

assert library.is_odd_or_even(3) == 'odd'
assert library.is_odd_or_even(8) == 'even'
assert library.is_odd_or_even(0) == 'even'
assert library.is_odd_or_even(-1) == 'odd'
assert library.is_odd_or_even(-4) == 'even'
