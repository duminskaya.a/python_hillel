'''
Сформуйте строку, яка містить певну інформацію про символ по його індексу в відомому слові.
Наприклад, "The [індекс символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
Слово та номер отримайте за допомогою input() або скористайтеся константою.
Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".
'''

def print_index (symbol: str, word: str) -> None:
    if symbol in word:
        print(f"The '{word.index(symbol)}' symbol in '{word}' is '{symbol}'")
    else:
        print("Введіть дані ще раз.")


if __name__ == '__main__':
    symbol_to_check = input("Введіть букву тут: ")
    word_to_check = input("Введіть слово тут: ")
    print_index(symbol_to_check, word_to_check)


'''
Ввести з консолі строку зі слів за допомогою input() (або скористайтеся константою). Напишіть код, який визначить кількість слів, в цих даних.
'''

text = input()
result = len(text.split())
print("У цій строці " + str(result) + " слів.")


'''
Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']. 
Напишіть код, який сформує новий list (наприклад lst2), який би містив всі числові змінні (int, float), які є в lst1. 
Майте на увазі, що дані в lst1 не є статичними можуть змінюватись від запуску до запуску.
'''


def simple_get_new_numeric_list(lst: list) -> list:
    new_lst = []
    for i in lst:
        if type(i) in [int, float]:
            new_lst.append(i)
    return new_lst


def not_simple_get_new_numeric_list(lst: list) -> list:
    return [i for i in lst if type(i) in [int, float]]


if __name__ == '__main__':
    lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
    result = not_simple_get_new_numeric_list(lst1)
    print(result)


